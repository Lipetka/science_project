\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}Measurement preparation}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}PYNQ development board}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Measured signals}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}FPGA circuit}{3}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Incoming data generator}{3}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.5}Expected result}{4}{subsection.2.5}%
\contentsline {section}{\numberline {3}Measurement}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}Process}{4}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Results}{4}{subsection.3.2}%
\contentsline {section}{\numberline {4}Conclusion}{7}{section.4}%
