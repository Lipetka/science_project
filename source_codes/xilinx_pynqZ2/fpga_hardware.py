# pylint: disable=import-error
"""This file contains class responsible for communicating with FPGA
side of PYNQ Z2 (Xilinx) microcontroller. 
"""
import time
import numpy as np

import pynq
from pynq import allocate, Overlay
import pynq.lib.video as pynq_video_lib


class FpgaHardware:
    """
    Manages FPGA settings, overlays, VDMA operations, and frame capture.

    Attributes:
    - color_format_options (list)[
            - "RGB888"
    ]: Available color format options.
    - image_formats (list)[
            - "bmp"
            - "jpeg"
    ]: Available image format options.
    - fpga_settings (dict)[
            - "overlay_set" : 0, # hold the overlay setup flag
            - "vdma_running" : 0, # flag if VDMA is running
            - "height" : 0, # height of display with over and under flow
            - "width" : 0, # width of display with over and under flow
            - "color_format" : "not specified", # chosen color format
            - "bites_per_pixel" : 0, # set automatically when setting color format
            - "detected_height": 0, # detected resolution height
            - "detected_width" : 0 # detected resolution width
    ]: Dictionary holding various FPGA settings.
    - bitstreams_folder (str) = path to folder containing bitstreams


    Methods:
    - set_color_format(color_format: str) -> int: Sets the color format for FPGA processing.
    - set_resolution(width: int, height: int): Sets the resolution for FPGA processing.
    - set_overlay(): Configures and sets the FPGA overlay based on the current settings.
    - start_vdma(): Starts the VDMA (Video Direct Memory Access) read channel.
    - stop_vdma(): Stops the VDMA read channel.
    - reset_server(): Resets the server state, stopping the VDMA and resetting FPGA settings.
    - read_frame(format: str) -> frame: Reads a single frame from the VDMA read channel.
    - read_video(format: str, frames: int, fps: int) -> Tuple[list, list, list]: 
    Reads a video by capturing frames at a given FPS.
    """
    def __init__(self):
        # ininitiate the FpgaHardware with empty variables
        #self.overlay  = None
        #self.vdma = None
        #self.frame = None
        #self.display_info_ip = None
        self.fpga_settings = {
            "overlay_set" : False, # hold the overlay setup flag
            "vdma_running" : False, # flag if VDMA is running
            "height" : 0, # height of display with over and under flow
            "width" : 0, # width of display with over and under flow
            "color_format" : "not specified", # chosen color format
            "bites_per_pixel" : 0, # set automatically when setting color format
            "detected_height": 0, # detected resolution height
            "detected_width" : 0 # detected resolution width
        }
        self.color_format_options = ["RGB888"]
        self.image_formats = ["bmp","jpeg"]
        self.bitstreams_folder = "bitstreams_official"
        self.front_porch = 0
        self.top_porch = 0

    async def detect_resolution(self):
        """Reads display resolution from an FPGA overlay and sets it in the application.

        This function performs the following steps:
        1. Checks if the FPGA overlay is set, and if not, 
        sets it using a default bitstream.
        2. Assigns the display_info_ip attribute from the overlay, 
        assuming it has a dispCheck_0 IP core.
        3. Reads the detected height and width from the display_info_ip registers 
        and updates the FPGA settings.
        
        If any error occurs during the process,
        a custom FpgaServerException is raised,
        providing details about the encountered error.

        Raises:
            - FpgaServerException: Raises exception to be later handled by API.
        """
        try:
            # if overlay is not set, set it
            if not self.fpga_settings["overlay_set"]:
                self.overlay = Overlay(f"{self.bitstreams_folder}/RGB888.bit")

            # assign display_info_ip
            self.display_info_ip = self.overlay.dispCheck_0

            # read resolution
            self.fpga_settings["detected_height"] = self.display_info_ip.read(0x00)
            self.fpga_settings["detected_width"] = self.display_info_ip.read(0x04)

        except Exception as error:
            raise FpgaServerException(status_code=500,
                                      detail="resolution could not be detected") from error

    def set_color_format(self, color_format:str):
        """Sets the color format for FPGA processing.

        This function takes a color_format argument, 
        which should be a string specifying the desired color format.
        It validates that the provided color_format is a string and is one 
        of the available options in self.color_format_options.
        If the validation is successful, it updates the FPGA settings 
        with the specified color format and related settings.


        Args:
            - color_format (str): Define color format to be used, 
            currently supports only RGB888

        Raises:
            - FpgaServerException: Custom exception with detailed message and status code
        """

        try:
            if not (isinstance(color_format, str)) or color_format not in self.color_format_options:
                raise FpgaServerException(status_code=400,
                                    detail=f"Color_format must be: {self.color_format_options}")

            self.fpga_settings["color_format"] = color_format
            if color_format == "RGB888":
                self.fpga_settings["bites_per_pixel"] = 24
        except Exception as error:
            raise FpgaServerException(status_code=500,
                                detail=f"Error while setting color format: {error}") from error

    def set_resolution(self, width, height):
        """
        Sets the resolution for FPGA processing.

        This function takes width and height as arguments,
        validating that both are integers.
        If the validation is successful, it updates 
        the FPGA settings with the specified width and height.

        Parameters:
            - width (int): The desired width of frame.
            - height (int): The desired height of frame.

        Raises:
            - FpgaServerException: If either width or height is not an integer, 
            a 400 status code is raised with a detailed error message.
            - FpgaServerException: If any other error occurs during the process, 
            a 500 status code is raised with details about the error.
        """
        try:
            if not (isinstance(width, int)) or not isinstance(height, int):
                raise FpgaServerException(status_code=400,
                                          detail="Width and height must be integers")

            self.fpga_settings["width"] = width
            self.fpga_settings["height"] = height
        except Exception as error:
            raise FpgaServerException(status_code=500,
                                detail=f"Error while setting resolution: {error}") from error

    async def set_overlay(self):
        """
        Configures and sets the FPGA overlay based on the current settings.

        This asynchronous method performs the following steps:
        1. Checks if the color format is set in the FPGA settings.
        2. Validates that the resolution (width and height) is at least 1x1.
        3. Stops the VDMA (Video Direct Memory Access) if it is currently running.
        4. Attempts to set the FPGA overlay, 
        raising an FpgaServerException with a 500 status code if unsuccessful.
        5. Configures the VDMA with the specified width, height, and color format.
        6. Allocates memory for a frame based on the specified resolution and color format.

        Raises:
            - FpgaServerException: If the color format is not set or 
            the resolution is invalid, a 400 status code is raised.
            - FpgaServerException: If there is an error setting 
            the overlay or configuring the VDMA, a 500 status code is raised.
        """
        ## Check all settings
        # is color format set?
        if self.fpga_settings["color_format"] not in self.color_format_options:
            raise FpgaServerException(status_code=400, detail="color format not set")

        # is resolution at least 1,1?
        if self.fpga_settings["height"] == 0 or self.fpga_settings["width"] == 0:
            raise  FpgaServerException(status_code=400, detail="Width and height cannot be 0")

        # If VDMA is running, stop it
        if self.fpga_settings["vdma_running"]:
            self.stop_vdma()

        # try to set overlay and throw FPGA hardware error if overlay set fails
        try:
            self.overlay     = Overlay(f"{self.bitstreams_folder}/"
                                       f"{self.fpga_settings['color_format']}.bit")
            self.vdma        = self.overlay.axi_vdma_0     # get the vdma module
            self.display_info_ip = self.overlay.dispCheck_0
        except Exception as error:
            raise FpgaServerException(status_code=500,
                                detail=f"Overlay could not be set: {error}") from error

        try:
            self.vdma.readchannel.mode = pynq_video_lib.VideoMode(self.fpga_settings["width"],
                                                   self.fpga_settings["height"], 24)
            self.frame = allocate(shape=(self.fpga_settings["width"],
                                    self.fpga_settings["height"],
                                    3),
                                    dtype=np.uint8, cacheable = 0)
        except Exception as error:
            raise FpgaServerException(status_code=500,
                                detail=f"VDMA could not be set: {error}") from error

        self.fpga_settings["overlay_set"] = True

    def start_vdma(self):
        """
        Starts the VDMA (Video Direct Memory Access) read channel.

        This method attempts to start the VDMA read channel and 
        updates the FPGA settings accordingly.
        If the VDMA channel starts successfully, it returns 1; otherwise, 
        it raises an FpgaServerException with a 500 status code.

        Raises:
        - FpgaServerException: If the VDMA channel cannot be started or 
        an error occurs during the process, a 500 status code is raised
        with details about the error.
        """
        try:
            self.vdma.readchannel.start()
            if self.vdma.readchannel.running:
                self.fpga_settings["vdma_running"] = True
            else:
                self.fpga_settings["vdma_running"] = False
                raise FpgaServerException(status_code=500,
                                    detail="VDMA channel could not be started")
        except Exception as error:
            raise FpgaServerException(status_code=500,
                                detail=f"VDMA channel could not be started: {error}") from error

    def stop_vdma(self):
        """
        Stops the VDMA (Video Direct Memory Access) read channel.

        This method attempts to stop the VDMA read channel and 
        updates the FPGA settings accordingly.
        If the VDMA channel stops successfully, it returns 1; otherwise, 
        it raises an FpgaServerException with a 500 status code.

        Raises:
        - FpgaServerException: If the VDMA channel cannot be stopped or 
        an error occurs during the process, a 500 status code is raised
        with details about the error.
        """
        try:
            self.vdma.readchannel.stop()
            if self.vdma.readchannel.running:
                self.fpga_settings["vdma_running"] = True
                raise FpgaServerException(status_code=500,
                                          detail="VDMA channel could not be started")
            self.fpga_settings["vdma_running"] = False
        except Exception as error:
            raise FpgaServerException(status_code=500,
                                detail=f"VDMA channel could not be started: {error}") from error

    def reset_server(self):
        """
        Resets the server state, stopping the VDMA and resetting FPGA settings.

        This method performs the following actions:
        1. Stops the VDMA (Video Direct Memory Access).
        2. Resets the FPGA settings to default values, 
        clearing overlay setup and VDMA running flags.
        The settings include overlay_set, vdma_running, 
        height, width, color_format, bites_per_pixel,
        detected_height, and detected_width.

        Raises:
        - FpgaServerException: If an error occurs during the process, 
        a 500 status code is raised with details about the error.
        """
        try:
            self.stop_vdma()
            self.fpga_settings = {
                "overlay_set" : False, # hold the overlay setup flag
                "vdma_running" : False, # flag if VDMA is running
                "height" : 0, # height of display with over and under flow
                "width" : 0, # width of display with over and under flow
                "color_format" : "not specified", # chosen color format
                "bites_per_pixel" : 0, # set automatically when setting color format
                "detected_height": 0, # detected resolution height
                "detected_width" : 0 # detected resolution width
            }
        except Exception as error:
            raise FpgaServerException(status_code=500,
                                detail="Could not reset server: {error}") from error

    async def read_frame(self, frame_format):
        """
        Reads a frame from the VDMA (Video Direct Memory Access) read channel.

        This asynchronous method performs the following checks before attempting to read the frame:
        1. Validates that the specified image format is a string and one of the available options.
        2. Ensures that the resolution is at least 1x1.
        3. Checks if the FPGA overlay is set.
        4. Verifies that the VDMA is currently running.

        If all checks pass, it attempts to read a frame asynchronously using the VDMA read channel.

        Parameters:
        - frame_format (str): The desired image format for the frame. 
        Must be one of the available options. ("bmp","jpeg")

        Returns:
        - frame: The read frame.

        Raises:
        - FpgaServerException: If any of the checks fail, 
        a 400 status code is raised with a detailed error message.
        - FpgaServerException: If there is an error reading the frame or 
        any other error occurs during the process,
        a 500 status code is raised with details about the error.
        """
        ## Check all settings
        # is color format set?
        if not (isinstance(frame_format, str)) or frame_format not in self.image_formats:
            raise FpgaServerException(status_code=400,
                                      detail=f"Format must be: {self.image_formats}")
        # is resolution at least 1,1?
        if self.fpga_settings["height"] == 0 or self.fpga_settings["width"] == 0:
            raise FpgaServerException(status_code=400,
                                      detail="Width and height cannot be set to 0")
        # is overlay set?
        if not self.fpga_settings["overlay_set"]:
            raise FpgaServerException(status_code=400, detail="Overlay not set")
        # is VDMA running?
        if not self.fpga_settings["vdma_running"]:
            raise FpgaServerException(status_code=400, detail="VDMA is not running")

        try:
            frame = await self.vdma.readchannel.readframe_async()
        except Exception as error:
            raise FpgaServerException(status_code=500,
            detail=f"Error while reading frame: {error}") from error

        return frame

    async def read_video(self,frames, fps, frame_format):
        """
        Reads a video by capturing a specified number of frames at a given frames-per-second (FPS).

        This asynchronous method performs the following checks before attempting to read the video:
        1. Validates that the specified image format is a string and one of the available options.
        2. Ensures that the number of frames is an integer and more than 2.
        3. Validates that the FPS is an integer and more than 0.
        4. Checks if the resolution is set (height and width are not 0).
        5. Checks if the FPGA overlay is set.
        6. Verifies that the VDMA is currently running.

        The method then preallocates memory for frames, 
        calculates the required timing to meet FPS specifications,
        and captures frames using precise timing.

        Parameters:
        - frame_format (str): The desired image format for the frames. 
        Must be one of the available options. ("bmp","jpeg")
        - frames (int): The number of frames to capture.
        - fps (int): The desired frames-per-second.

        Returns:
        - actual_fps (list): A list of actual FPS counts for each captured frame.
        - frame_gather_list (list): A list of the time taken to gather each frame.
        - video_frames (list): A list containing the captured frames.

        Raises:
        - FpgaServerException: If any of the checks fail or an error occurs during the process, 
        a 400 or 500 status code
        is raised with a detailed error message.
        """
        ## check for settings
        # if format correct format?
        if not (isinstance(frame_format, str)) or frame_format not in self.image_formats:
            raise FpgaServerException(status_code=400,
                                      detail=f"Format must be: {self.image_formats}")
        # is frames correct format?
        if not (isinstance(frames, int)) or frames < 2:
            raise FpgaServerException(status_code=400,
                                      detail="Frames must be integer and more than 2")
        # is fps correct format?
        if not (isinstance(fps, int)) or fps == 0:
            raise FpgaServerException(status_code=400,
            detail="FPS must be integer and more than 0")
        # is resolution set?
        if self.fpga_settings["height"] == 0 or self.fpga_settings["width"] == 0:
            raise FpgaServerException(status_code=400,
            detail="width or height cannot be set to 0")
        # is overlay set?
        if not self.fpga_settings["overlay_set"]:
            raise FpgaServerException(status_code=400, detail="Overlay not set")
        # is vdma running?
        if not self.fpga_settings["vdma_running"]:
            raise FpgaServerException(status_code=400, detail="VDMA is not running")

        video_frames = [self.fpga_settings["width"],
                        self.fpga_settings["height"],3] * frames # preallocate memory for frames
        actual_fps_list = [] # list for actual FPS count
        frame_gather_list = [] # list for gathered frames

        try:
            # calculate required timing to meet fps specifications
            time_period = (1/fps)*1E9
            # FPS is handled by precise timing using time.time_ns()
            for i in range(frames):
                time_start = time.time_ns()  # start fps timer

                frame_gather = time.time_ns() # start frame gathering timer
                frame = await self.vdma.readchannel.readframe_async() # get frame form VDMA
                frame_gather_list.append(time.time_ns()
                                         - frame_gather) # how long it took to get the frame

                video_frames[i] = frame # save the frame to videoFrames list

                # handle FPS stability
                if time_period < time.time_ns()-time_start:
                    actual_fps = 1/(time.time_ns()-time_start)*1E9
                    actual_fps_list.append(actual_fps) # type: ignore
                else:
                    actual_fps_list.append(fps) # type: ignore
                    while time.time_ns()-time_start < time_period:
                        continue
        except Exception as error:
            raise FpgaServerException(status_code=500,
                                detail=f"Error while reading video: {error}") from error

        return actual_fps_list, frame_gather_list, video_frames


class FpgaServerException(Exception):
    """
    Custom exception for Fpga server manager, this exception sends status code
    to be used in HTTP api or other types of protocols, used codes are HTTP status codes
    this exception also passes custom message to be displayed to client

    400 - user entered wrong input
    500 - internal server error
    """
    def __init__(self, status_code=500, detail="No details"):
        self.status_code = status_code
        self.detail = detail
        super().__init__(self.status_code, self.detail)
