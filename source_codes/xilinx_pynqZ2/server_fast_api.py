"""Module providing API to control display sniffer board"""
# Standard imports
import base64
import io

# Imports for fastAPI
from PIL import Image
from fastapi import FastAPI, HTTPException
from fastapi import Response
from starlette.responses import JSONResponse

# imports for FPGA
from fpga_hardware import FpgaHardware, FpgaServerException


# initiate classes
app = FastAPI()
fpga_hardware = FpgaHardware() # hold variables and control functions for fpga hardware

# HOME SCREEN ------------------------------------

@app.get('/')
async def hello():
    """
    Detects resolution using `detect_resolution()` and returns FPGA hardware settings.

    Returns:
    - dict: FPGA hardware settings.
    """
    try:
        await detect_resolution()
    except FpgaServerException as fpga_error:
        raise HTTPException(status_code=fpga_error.status_code,
                                detail=fpga_error.detail
                                ) from fpga_error

    return fpga_hardware.fpga_settings

# SET COLOR FORMAT ------------------------------------

@app.get('/setColorFormat/colorFormat={color_format}')
def set_color_format(color_format:str):
    """
    Sets the color format on the FPGA hardware.

    Parameters:
    - color_format (str): The desired color format.

    Returns:
    - int: 1 if the color format is successfully set.
    """
    try:
        fpga_hardware.set_color_format(color_format)
    except FpgaServerException as fpga_error:
        raise HTTPException(status_code=fpga_error.status_code,
                                detail=fpga_error.detail
                                ) from fpga_error
    return 1

# SET HEIGHT and WIDTH ------------------------------------
@app.get('/setResolution/displayWidth={width}/displayHeight={height}')
def set_resolution(height:int, width:int):
    """
    Sets the resolution of the FPGA hardware.

    Parameters:
    - height (int): The height of the resolution.
    - width (int): The width of the resolution.

    Returns:
    - int: 1 if the resolution is successfully set.
    """
    try:
        fpga_hardware.set_resolution(width, height)
    except FpgaServerException as fpga_error:
        raise HTTPException(status_code=fpga_error.status_code,
                                detail=fpga_error.detail
                                ) from fpga_error
    return 1

# SET OVERLAY ------------------------------------
@app.get('/setOverlay')
async def set_overlay():
    """
    Sets the overlay on the FPGA hardware.

        Returns:
    - int: 1 if the overlay setting was successful.
    """
    try:
        await fpga_hardware.set_overlay()
    except FpgaServerException as fpga_error:
        raise HTTPException(status_code=fpga_error.status_code,
                                detail=fpga_error.detail
                                ) from fpga_error
    return 1

# START VDMA ------------------------------------
@app.get('/startVDMA')
def start_vdma():
    """
    Starts the VDMA (Video Direct Memory Access) on the FPGA hardware.

        Raises:
    - HTTPException: If VDMA fails to start successfully.

        Returns:
    - int: 1 if the start operation was successful.
    """
    try:
        fpga_hardware.start_vdma()
    except FpgaServerException as fpga_error:
        raise HTTPException(status_code=fpga_error.status_code,
                                detail=fpga_error.detail
                                ) from fpga_error
    return 1

# STOP VDMA ------------------------------------
@app.get('/stopVDMA')
def stop_vdma():
    """
    Stops the VDMA (Video Direct Memory Access) on the FPGA hardware.

        Returns:
    - int: 1 if the stop operation was successful.
    """
    try:
        fpga_hardware.stop_vdma()
    except FpgaServerException as fpga_error:
        raise HTTPException(status_code=fpga_error.status_code,
                                detail=fpga_error.detail
                                ) from fpga_error
    return 1

# READ VIDEO ------------------------------------
@app.get('/readVideo/frames={frames}/fps={fps}/image_format={image_format}')
async def read_video(frames: int, fps: int, image_format:str):
    """
    Reads video frames from an FPGA hardware device, 
    calculates frame rates, 
    and returns the frames and associated metadata.

        Parameters:
    - frames (int): The number of frames to read from the FPGA.
    - fps (int): The desired frames per second (FPS) for the video.
    - image_format (str): The desired image format for the output frames ('bmp' or 'jpeg').

        Returns:
    Tuple:
    - actual_fps_list (List[int]): A list of actual frame rates achieved during video capture.
    - frame_gather_list (List[Any]): A list containing information about the gathered frames.
    - video_frames (List[ndarray]): A list of video frames captured from the FPGA.

        Raises:
    - HTTPException(500): If an error occurs during video capture, processing, or encoding.
      The exception detail provides information about the specific error.
    """

    try:
        actual_fps_list, frame_gather_list, video_frames = await fpga_hardware.read_video(frames,
                                                                                    fps,
                                                                                    image_format)
    except FpgaServerException as fpga_error:
        raise HTTPException(status_code=fpga_error.status_code,
                                detail=fpga_error.detail
                                ) from fpga_error

    fps_result = {
        "averageFPS" : sum(actual_fps_list[1:])/len(actual_fps_list[1:]), # type: ignore
        "max FPS" : max(actual_fps_list), # type: ignore
        "min FPS": min(actual_fps_list), # type: ignore
        "FPS list": actual_fps_list,
        "FrameGather" : frame_gather_list
    }

    frames_to_send = []

    # video frames have all frames stored
    for i in range(frames):
        image = Image.fromarray(video_frames[i])
        frames_to_send.append(image)

    # Convert PIL images to base64-encoded strings
    image_base64_list = []
    for image in frames_to_send:

        try:
            image_bytes = io.BytesIO()
            image.save(image_bytes, format="BMP")
            image_base64 = base64.b64encode(image_bytes.getvalue()).decode("utf-8")
            image_base64_list.append(image_base64)
        except (AttributeError, IOError, ValueError,
                MemoryError) as error:
            # Handle specific exceptions related to image processing or encoding
            raise HTTPException(status_code=500,
                detail=f"Image processing error: {error}") from error

    response_data = [
        fps_result, image_base64_list
    ]
    return JSONResponse(content = {"data": response_data})

# READ FRAME ------------------------------------
@app.get('/readFrame/image_format={image_format}')
async def read_frame(image_format: str):
    """
    Retrieves a frame from an FPGA hardware device, 
    performs postprocessing, 
    and returns the processed image as bytes.

    Parameters:
    - image_format (str): The desired image format for the output ('jpeg' or 'bmp').

    Returns:
    - Response: A FastAPI Response object containing the processed image as bytes.

    Raises:
    - HTTPException(500): If an error occurs during the image processing, 
    such as TypeErrors, ValueErrors,
    or OSErrors. The exception detail provides 
    information about the specific error.
    """
    ## retreive frame from FPGA
    try:
        frame = await fpga_hardware.read_frame(image_format)
    except FpgaServerException as fpga_error:
        raise HTTPException(status_code=fpga_error.status_code,
                                detail=fpga_error.detail
                                ) from fpga_error

    ## postprocessing
    # Convert the Image object to bytes
    try:
        image = Image.fromarray(frame)
    except TypeError as error:
        raise HTTPException(status_code=500, detail=f"{error}") from error
    except ValueError as error:
        raise HTTPException(status_code=500, detail=f"{error}") from error

    # save image to an in-memory bytes buffer
    try:
        if image_format == "jpeg":
            with io.BytesIO() as buf:
                image.save(buf, format='JPEG')
                image_bytes = buf.getvalue()
            return Response(image_bytes, media_type = "image/jpeg")
        elif image_format == "bmp":
            with io.BytesIO() as buf:
                image.save(buf, format='BMP')
                image_bytes = buf.getvalue()
            return Response(image_bytes, media_type = "image/bmp")

    except ValueError as error:
        raise HTTPException(status_code=500, detail=f"{error}") from error
    except OSError as error:
        raise HTTPException(status_code=500, detail=f"{error}") from error

# DETECT RESOLUTION -----------------------------------
@app.get('/detectResolution')
async def detect_resolution():
    """Function to read fpga registers to read detected resolution

        Returns:
    - int: 1 if detection was successfull
    """
    try:
        await fpga_hardware.detect_resolution()
    except FpgaServerException as fpga_error:
        raise HTTPException(status_code=fpga_error.status_code,
                                detail=fpga_error.detail
                                ) from fpga_error
    return 1

# RESET SERRVER ---------------------------------------
@app.get('/softReset')
def reset_server():
    """Resets the settings of server

        Returns:
    - int: 1 if reset was successfull
    """
    try:
        fpga_hardware.reset_server()
    except FpgaServerException as fpga_error:
        raise HTTPException(status_code=fpga_error.status_code,
                                detail=fpga_error.detail
                                ) from fpga_error
    return 1

# GET ACCEPTABLE SETTINGS --------------------------------
@app.get('/getPossibleSettings')
def get_possible_settings():
    """Returns currently supported settings of server

        Returns:
    - dict: Dictionary of supported options ->
        - color_formats: supported color formats
        - image_formats: supported image formats
        - bitsream_folder: folder where bitsreams are stored
    """
    returned_dictionary = {
        "color_formats" : fpga_hardware.color_format_options,
        "image_formats" : fpga_hardware.image_formats,
        "bitsream_folder" : fpga_hardware.bitstreams_folder
    }

    return returned_dictionary
